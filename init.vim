" Options
set background=dark
set clipboard=unnamedplus
set completeopt=noinsert,menuone,noselect
set hidden
set number
set relativenumber
set title
set ttimeoutlen=0
set wildmenu
set noerrorbells
set nowrap
set nohlsearch
set scrolloff=8
set signcolumn=yes
set modifiable

" Tabs size
set expandtab
set shiftwidth=2
set tabstop=2 softtabstop=2

" Leader key
let mapleader = " "

call plug#begin()
  Plug 'vim-airline/vim-airline'
  Plug 'morhetz/gruvbox'
  Plug 'sheerun/vim-polyglot'
  Plug 'jiangmiao/auto-pairs'
  Plug 'preservim/nerdtree'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'
  Plug 'tpope/vim-fugitive'
  Plug 'airblade/vim-gitgutter'
call plug#end()

" Vim airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_powerline_fonts = 1

" Gruvbox
colorscheme gruvbox

" NERDTree
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
nnoremap <expr> <C-f> g:NERDTree.IsOpen() ? "\:NERDTreeClose<CR>" : bufexists(expand('%')) ? "\:NERDTreeFind<CR>" : "\:NERDTree<CR>"

" Coc
nmap <silent> gd <Plug>(coc-definition)
let g:coc_global_extensions = ['coc-tslint-plugin', 'coc-tsserver', 'coc-emmet', 'coc-css', 'coc-json', 'coc-html', 'coc-yank', 'coc-prettier', 'coc-angular']
nmap <leader>qf  <Plug>(coc-fix-current)
xmap <leader>fc  <Plug>(coc-format-selected)
nmap <leader>fc  <Plug>(coc-format-selected)
" Fzf
nmap <leader>f :GFiles<CR>
nmap <leader>\ :Rg<CR>

" Buffers
nmap gt :bnext<CR>
nmap gT :bprevious<CR>
nmap gb <C-O>

" Tabs
nmap tn :tabnext<CR>
nmap tN :tabprevious<CR>
